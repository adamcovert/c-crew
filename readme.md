# Gulp starter-kit

[![devDependencies](https://david-dm.org/adamcovert/start-scss/dev-status.svg)](https://david-dm.org/adamcovert/start-scss?type=dev)
[![dependencies](https://david-dm.org/adamcovert/start-scss.svg)](https://david-dm.org/adamcovert/start-scss)

## Using in another project

1. `git clone git@github.com:adamcovert/start-scss.git your-folder-name`
2. `cd your-folder-name`
3. `git remote set-url origin git@github.com:user/repo.git`
4. `npm i`