$(document).ready(function() {

  svg4everybody();

  $('.s-intro__inner').removeClass('s-intro__inner--is-hidden')

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-open');
    document.body.style.position = 'fixed';
    document.body.style.top = `-${window.scrollY}px`;
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-open');
    var scrollY = document.body.style.top;
    document.body.style.position = '';
    document.body.style.top = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
  });

  var mySwiper = new Swiper('.swiper-container', {})

  $('.s-sidebar__documents').lightGallery({
    selector: 'a',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: false,
    animateThumb: false,
    showThumbByDefault: false
  });
});